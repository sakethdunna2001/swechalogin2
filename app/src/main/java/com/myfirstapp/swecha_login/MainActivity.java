package com.myfirstapp.swecha_login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.ktx.Firebase;

import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    
    private FirebaseAuth firebaseAuth ;
    private TextInputLayout userName;
    private TextInputLayout password;
    private TextInputLayout cpassword;
    private Button signInBtn;
    private FirebaseDatabase firebaseDatabase;
    
    private TextView signUpText;
    

    
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        
        userName = (TextInputLayout) findViewById(R.id.user_name_input);
        password = (TextInputLayout) findViewById(R.id.password_input);
        signInBtn = (Button) findViewById(R.id.signInBtn);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        signUpText = (TextView)findViewById(R.id.sign_up_text);


        if(firebaseAuth.getCurrentUser() != null){
            startActivity(new Intent(MainActivity.this,HomePage.class));
        }



        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String musername = userName.getEditText().getText().toString();
                String mpassword = password.getEditText().getText().toString();
                firebaseAuth.signInWithEmailAndPassword(musername,mpassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            Toast.makeText(MainActivity.this, "Logged in", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(MainActivity.this,HomePage.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(MainActivity.this, "Something went wrong.Try again", Toast.LENGTH_SHORT).show();
                            Log.d("SignInException",String.valueOf(task.getException()));
                        }
                    }
                });

            }
        });

        signUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SignupActivity.class);

                startActivity(intent);
            }
        });



        
        
    }
}