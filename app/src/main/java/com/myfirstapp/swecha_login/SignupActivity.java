package com.myfirstapp.swecha_login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity {

    private TextInputLayout nameInput,emailInput,passwordInput,confirmPasswordInput;

    private Button signUpBtn;

    private FirebaseAuth firebaseAuth;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        reference = firebaseDatabase.getReference();


        nameInput = (TextInputLayout) findViewById(R.id.name_input);


        emailInput = (TextInputLayout) findViewById(R.id.email_input);
        passwordInput = (TextInputLayout) findViewById(R.id.signup_password_input);
        confirmPasswordInput = (TextInputLayout) findViewById(R.id.confirm_password_input);
        signUpBtn = (Button) findViewById(R.id.signUpBtn);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String musername = emailInput.getEditText().getText().toString();
                String mpassword = passwordInput.getEditText().getText().toString();

                String cmpassword = confirmPasswordInput.getEditText().getText().toString();


                if (mpassword.equals(cmpassword)){
                    firebaseAuth.createUserWithEmailAndPassword(musername,mpassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(SignupActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                String name = nameInput.getEditText().getText().toString();

                                reference.child("Details").child("name").setValue(name);


                            }else{
                                Toast.makeText(SignupActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(SignupActivity.this, "Both password must be same", Toast.LENGTH_SHORT).show();

                }



            }
        });



    }
}